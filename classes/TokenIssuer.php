<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Classes;

use GuzzleHttp\Client;

class TokenIssuer
{
    const BASE_URI = 'https://testapi.citizen.is';
    const PRIVATE_API_KEY = '3608354f-8d73-4a86-be5f-d747eed254e5';
    const URI_PAYMENT_SESSION = '/v1/payments/payment-session';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getTransactionID()
    {
        $url = self::BASE_URI . self::URI_PAYMENT_SESSION;
        $headers = [
            'Content-Type' => 'application/json',
            'AuthorizationCitizen' => self::PRIVATE_API_KEY,
        ];
        $body = [
            "customerEmailAddress"=> "test@gmail.com",
            "merchantEmailAddress"=> "test1@gmail.com",
            "merchantInternalId" => "123455",
            "amount"=> "1.00",
            "currency"=> "GBP",
            "paymentGiro"=> "FPS",
            "shortReference"=> "yourRef",
            "detailedReference"=> "details",
            "customerDeviceOs"=> "Windows 11",
            "customerIpAddress"=> "37.48.34.149",
            "searchableText"=> "searchable text",
            "payloadEncrypted"=> "true",
            "successRedirectUrl"=> "https=>//www.example.com/success",
            "failureRedirectUrl"=> "https=>//www.example.com/failure"
        ];

        $res = $this->client->request('POST', $url, [
            'headers' => $headers,
            'body' => json_encode($body)]);

        $text = (string)$res->getBody();
        $transactionID = $text;
        return $transactionID;
    }


}
