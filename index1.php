<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

</head>
<body>
<h1 data-bind="style: {color: colorModel}">Привет <span data-bind="text: name"></span></h1>
<button id="displayBtn">Нажми</button>
<p data-bind="
visible: visibility,
html: htmlText,
css: { redStyle: classCss }
">text</p>
<!-- ko if: classCss -->
<p>if-binding</p>
<!-- /ko -->
Текущее время:<span data-bind="text: hour"></span>:<span data-bind="text: minute"></span>:<span data-bind="text: second"></span>.

<h2 data-bind="if: classCss">Список телефонов</h2>
<ul data-bind="foreach: phones">
    <p data-bind="text: $data.name"></p>

</ul>
<div id="content" data-bind="text: $element.id"></div>

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/knockout/knockout-3.4.0.js"></script>
<script type="text/javascript" src="main.js"></script>
</body>
</html>