function rundColor() {
return '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase();
}

var viewModel = {
    name: " мир!",
    hour: ko.observable(new Date().getHours()),
    minute: ko.observable(new Date().getMinutes()),
    second: ko.observable(new Date().getSeconds()),
    visibility: ko.observable(true),
    htmlText: ko.observable("<span style='color:"+rundColor()+";'>Привет KnockoutJS!</span>"),
    classCss: ko.observable(true),
    colorModel: ko.observable("red"),
    phones:  ko.observableArray([
        {
            name: "iPhone 6S",
            company: "Apple"
        },
        {
            name: "Lumia 950",
            company: "Microsoft"
        },
        {   name:"Nexus 6P",
            company: "Huawei"
        }])
};

ko.applyBindings(viewModel);

setInterval(
function() {
    viewModel.hour(new Date().getHours());
    viewModel.minute(new Date().getMinutes());
    viewModel.second(new Date().getSeconds());

    viewModel.htmlText("<span style='color:"+rundColor()+";'>Привет KnockoutJS!</span>");
}
, 1000);

a = viewModel.second.subscribe(
function(a) {
    console.log(a);

});
setTimeout(() => {
a.dispose();
}, 5000);


var displayBtn = document.getElementById("displayBtn");
displayBtn.addEventListener("click", function (e) {

    if(viewModel.colorModel()=="red")
        viewModel.colorModel("blue");
    else
        viewModel.colorModel("red");
    viewModel.classCss(!viewModel.classCss());

});
